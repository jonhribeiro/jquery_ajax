function consultaCep(params) {
    // mostrando a barra de progresso ao clicar no botao de pesquisar cep
    $(".barra-progresso").show()
    const cep = document.getElementById("cep").value;

    $.ajax({
        url: `http://viacep.com.br/ws/${cep}/json/`,
        type: "GET",
        success: function(response) {
            // 1º forma de preencher no index com dado pego com ajax
            document.getElementById("logradouro").innerHTML = response.logradouro;
            // 2º forma de preencher no index
            $("#ceps").html(response.cep);
            $("#complemento").html(response.complemento);
            $("#bairro").html(response.bairro);
            $("#localidade").html(response.localidade);
            $("#uf").html(response.uf);
            // apresendo os dado da tabela com class cep
            $(".cep").show()
                // oculto a barra de progresso
            $(".barra-progresso").hide()
                // com essa linha eu limpo o campo de digitar o cep
            $("#cep").val('');

        }
    });
}
$(function() {
    // iniciando com dado da tabele e barra de progresso oculta
    $(".cep").hide()
    $(".barra-progresso").hide()
})

function somar(params) {
    const a = document.getElementById("aa").value
    const b = document.getElementById("be").value
    console.log(a);
    alert((parseInt(a) + parseInt(b)))

}